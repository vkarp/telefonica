/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.telefonica.bookstore.core.jalo;

/**
 * Size variant of apparel product.
 */
public class ApparelSizeVariantProduct extends GeneratedApparelSizeVariantProduct
{
	// Deliberately empty class
}
