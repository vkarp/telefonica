/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 12, 2020, 11:19:15 AM                   ---
 * ----------------------------------------------------------------
 */
package com.telefonica.bookstore.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedBookstoreFacadesConstants
{
	public static final String EXTENSIONNAME = "bookstorefacades";
	
	protected GeneratedBookstoreFacadesConstants()
	{
		// private constructor
	}
	
	
}
